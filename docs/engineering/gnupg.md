
![GNU Privacy Guard](../img/gnupg.png)

We use [GNU Privacy Guard](https://www.gnupg.org/) which is a complete and free implementation of the OpenPGP standard.

##### Reference

- [User Handbook](https://www.gnupg.org/gph/en/manual.html)
- [Manual](https://www.gnupg.org/documentation/manuals/gnupg/)
- [Web Key Directory](https://wiki.gnupg.org/WKD)
- [Key Signing Party](https://www.cryptnet.net/fdp/crypto/keysigning_party/en/keysigning_party.html)


<hr />
image credit: GNU Privacy Guard project.