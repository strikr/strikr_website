
![Strikr Free Software project](../../img/logo_strikr.png)

##### Community Kickstart Meet

- date: 2021-07-17
- time: (UTC+0000) 0800 hrs
- type: saturday meet

##### Overview

The focus of this meet is to invite all interested folks for a quick update.

- what is the road ahead
- what are the upcoming activities
- who all are we reaching out to in the free software community
- action items


##### Further Reading

- [Goals](https://strikr.io/governance/goals/)
- [Baseline](https://strikr.io/engineering/baseline/)
- [Website Git Repo](https://codeberg.org/strikr/strikr_website)



##### Help

Need help ? have queries ? Please write to [foss@strikr.io](mailto:foss@strikr.io) for further assistance.


<hr />
image credit: Strikr Free Sofware project.