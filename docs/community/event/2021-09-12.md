
![LISP](../../img/lisp.png)

##### LISP

- date: 2021-09-12
- time: (UTC+0000) 0800 hrs
- type: WADR
- services
    - [#strikr](../irc.md)
    - [foss@strikr.io](../list.md)
    - [meet.jit.si/strikr](../jitsi.md)


##### Overview

Today, common lisp is an embodiment of LISP as a multi-paradigm, procedural, functional, reflective, meta general programming environment with a unique Common Lisp Object system (CLOS).

This is a practical deep-dive into using LISP capabilities to implement data structures and algorithms relevant to some of the Strikr project centric usecases.


##### Reference

- [LISP](https://en.wikipedia.org/wiki/Lisp_(programming_language))
- Mark Watson, [Loving Common Lisp](https://leanpub.com/lovinglisp/read) (Chap 2, 4, 5, 6, 8)
- [LISP Quickstart](https://cs.gmu.edu/~sean/lisp/LispTutorial.html)
- [LISP Books](https://lisp-lang.org/books/)
- [Common LISP HyperSpec](http://clhs.lisp.se/Front/Contents.htm)

---





<hr />
image credit: [The Perl Foundation](https://www.perlfoundation.org/)