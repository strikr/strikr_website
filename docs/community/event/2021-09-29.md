
![LISP](../../img/lisp.png)

##### LISP

- date: 2021-09-29
- time: (UTC+0000) 0230 hrs
- type: Wednesday meet
- services
    - [#strikr](../irc.md)
    - [foss@strikr.io](../list.md)
    - [meet.jit.si/strikr](../jitsi.md)


##### Overview

This is a continuation of our discussion about LISP capabilities to implement data structures and algorithms. We use the 'cons' model.


##### Reference

- [cons](https://en.wikipedia.org/wiki/Cons)
- Mark Watson, [Loving Common Lisp](https://leanpub.com/lovinglisp/read) (Chap 2, 4, 5, 6, 8)
- [Common LISP HyperSpec](http://clhs.lisp.se/Front/Contents.htm)

---





<hr />
image credit: LISP