
![Strikr Community Saturday Meet](../img/tux_community.png)

##### Saturday Meet

We focus on a particular topic, technology, programming language, aspect of concern or key point that needs to be discussed so that a decision can be taken. The topics could be expository in nature as well. We also organize skill enhancement sessions to rally the community together to become aware or rally around a strategic topic of tactical interest.

Whatever we discuss has a strong overlap with software freedom principles and has to align with the goals of the project.


##### Frequency

we meet twice a month on

- 1st Saturday
- 3rd Saturday

Sometimes, in order to catch up, we organize a mid-week meet on

- Wednesday

##### Timings

- Global (UTC+0000) 0400 - 0500 hrs
- City specific [timings](timings.md)


##### Events

Please checkout the [list of upcoming events](event/index.md)


#### Help

Need help ? have queries ? Please write to [foss@strikr.io](mailto:foss@strikr.io) for further assistance.
