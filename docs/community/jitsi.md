
![Strikr JITSI](../img/jitsi.png)

*All* Strikr free software community technology meets happen *online* thanks to Jitsi free software.

With the the power of websockets and real-time communication we are able to engage the community using

- audio
- video
- screen sharing


##### How to connect

It is very easy. You may connect using your Android phone and/or a browser on your system. We strongly recommend connecting using both your phone (for audio) and your system (browser for screen sharing).


- Browser
  While connecting from your system, please visit the URL [https://meet.jit.si/strikr](https://meet.jit.si/strikr)
- Android
  we have tested with version 19.4.3 and 20.0.3 APK files.
  You may download the APK files from [https://apkpure.com/jitsi-meet/org.jitsi.meet](https://apkpure.com/jitsi-meet/org.jitsi.meet)


##### Details

- room: strikr
- pass: agplv3
- lobby mode is enabled. please click 'ask to join'


##### Help

Need help ? have queries ? Please write to [foss@strikr.io](mailto:foss@strikr.io) for further assistance.


<hr />
image credit: jitsi project.