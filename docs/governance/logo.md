
![Strikr logo](../img/logo_strikr.png)

##### Strikr Logo

The stylized Strikr logo, is distinct with a slightly forward slanted upper case character 'S' in navy blue color (indicating bias for action or take a bold initiative) placed on top of a red colored pickaxe drawn horizontally. The spike is buried pick down to provide secure anchorage (indicating yield no quarter) and same pointed pick can be used to make new holes and advance (indicating resolute advancement in challenging conditions). 

The logo is a copyright of Strikr Free Software Project.


##### Logo Usage Guidelines

The logo must be used in square dimensions of 300x300, 200x200, 100x100, 64x64 pixels size.

**300x300** pixels

![Strikr logo 400x400px](../img/logo_strikr_300x300px.png)


**200x200** pixels

![Strikr logo 200x200px](../img/logo_strikr_200x200px.png)


**100x100** pixels

![Strikr logo 100x100px](../img/logo_strikr_100x100px.png)


**64x64** pixels

![Strikr logo 64x64px](../img/logo_strikr_64x64px.png)


##### How to Attribute

To attribute when using Strikr logo on your website, please insert a URL link ```https://strikr.io/``` at the point of usage of the logo.