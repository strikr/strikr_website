
![Strikr logo](../img/logo_strikr.png)

##### Private Enterprise Number

IANA assigned Private Enterprise Number (PEN) for Strikr is ```50276```.

PEN is used to create OID which is used in MIB entries associated with SNMP, LDAP, DHCP among other network protocols including ASN.1.


