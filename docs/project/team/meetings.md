
Project meetings are attended by the project members only.

##### Specifics

- Time (UTC+0000) 0400 hrs.
- Type CONTRIB | DODG | AÇIK
- Duration 120 minutes.
- We use [JITSI](../../community/jitsi.md).
- [Code of Conduct](../../governance/conduct.md) applies.
- Right to admission is reserved.


##### Schedule

| Date      | Type    | Topic, Focus Area                                                |
|:----------|:--------|:-----------------------------------------------------------------|
| 2021-07-31| CONTRIB | GNU Privacy Guard (GPG), Key Ring                                |
