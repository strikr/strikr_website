
Strikr Free Software Project | Software Freedom Foundation | Choice, Freedom and Equality | https://strikr.io | #strikr


OVERVIEW

as part of Strikr engineering process we use /opt directory for software installation. we avoid using dot directories (.dir), hidden directories or files. the less we depend on the system package manager the better.


SETUP

```
export PYTHONUSERBASE=/opt/py
```

Please ```pip install``` the following packages

* mkdocs                     1.2.1
* mkdocs-minify-plugin       0.4.0
* mkdocs-sequence-js-plugin  1.0.0
* mkdocs-static-i18n         0.17
* mkdocs-theme-bootstrap4    0.4.0
* pymdown-extensions         8.2


USAGE

Please see the ```scripts``` directory for the live, build and update scripts.


ACKNOWLEDGEMENT

* Tom Christie for the very useful MKdocs static site generator.
* Waylan Limberg for the discussions related to aspects of mkdocs usage.


LEGAL

strikr website content.

* all authored markdown, visual content is licensed under ```GFDL-1.3-or-later```
* all authored source code is licensed under ```AGPL-3.0-or-later```


HELP

please write to ```foss@strikr.io``` community mailing list.

